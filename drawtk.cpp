#include "drawtk.h"
#include "math.h"

namespace dohoa2d {

void DrawTK::setLayer(Layer *l)
{
    layer = l;
}

Layer *DrawTK::getLayer() const
{
    return layer;
}

void DrawTK::setStyle(Style *s)
{
    style = s;
}

Style *DrawTK::getStyle() const
{
    return style;
}

void DrawTK::setPolygonDrawer(IPolygonDrawer *drawer)
{
    poly_drawer = drawer;
}

void DrawTK::setLineDrawer(ILineDrawer *drawer)
{
    line_drawer = drawer;
}

void DrawTK::DrawLine(const Point &from, const Point &to, END_CAPS_TYPE type) const
{
    line_drawer->DrawLine(from, to, type,layer,static_cast<LineStyle*>(style));
}

void DrawTK::DrawRectangle(const Rect &rect) const
{
    if (!layer || !style) return;
    ShapeStyle* st = static_cast<ShapeStyle*>(style);
    layer->reserve(layer->Count() + rect.height*rect.width);
    for(int i = 0; i < rect.height; i++)
        for(int j = 0; j < rect.width; j++)
            if (i < st->border_width || i > rect.height -  st->border_width - 1 ||
                j < st->border_width || j > rect.width -  st->border_width - 1)
                layer->pushPixel(rect.p.x + j,rect.p.y + i,st->pen_color);
            else
                layer->pushPixel(rect.p.x + j,rect.p.y + i,st->brush_color);
}

void DrawTK::DrawEclipse(const Point center, const unsigned short &rx, const unsigned short &ry) const
{
    if (!layer || !style) return;
    ShapeStyle* st = static_cast<ShapeStyle*>(style);
    layer->reserve(layer->Count() + rx*ry);
    int rx2 = rx*rx;
    int ry2 = ry*ry;
    int rx_b2 = (rx - st->border_width)*(rx - st->border_width);
    int ry_b2 = (ry - st->border_width)*(ry - st->border_width);
    for(int i = -rx; i <=rx; i++)
    {
        int i2 = i*i;
        for(int j = -ry; j <= ry; j++)
        {
            int j2 = j*j;
            if (i2*ry2 + j2*rx2 <= rx2*ry2)
            {
                if (i2*ry_b2 + j2*rx_b2 >= rx_b2*ry_b2)
                    layer->pushPixel(center.x + i,center.y + j,st->pen_color);
                else
                    layer->pushPixel(center.x + i,center.y + j,st->brush_color);
            }
        }
    }
}

void DrawTK::DrawCircle(const Point center, const unsigned short &r) const
{
    if (!layer || !style) return;
    ShapeStyle* st = static_cast<ShapeStyle*>(style);
    layer->reserve(layer->Count() + r*r);
    int r2 = r*r;
    int rb2 = (r - st->border_width)*(r - st->border_width);
    for(int i = -r; i <= r; i++)
    {
        int i2 = i*i;
        for(int j = -r; j <= r; j++)
        {
            int rij2 = i2 + j*j;
            if (rij2 <= r2)
            {
                if (rij2 >= rb2)
                    layer->pushPixel(center.x + i,center.y + j,st->pen_color);
                else
                    layer->pushPixel(center.x + i,center.y + j,st->brush_color);
            }
        }
    }
}

void DrawTK::FillPolygon(Point *point_set, const unsigned short &nPnt, bool isConvex) const
{
    poly_drawer->DrawPolygon(point_set,nPnt,layer,static_cast<ShapeStyle*>(style),isConvex);
}

void LineDrawer::DrawLine(const Point &from, const Point &to, END_CAPS_TYPE type, Layer *layer, LineStyle *style)
{
    if (style->line_width <= 1)
        DrawThinLine(from, to, layer, style->pen_color);
    else
        DrawThickLine(from, to, layer, style->line_width, style->border_width, style->pen_color, style->brush_color);
}

// Assumption: from.x < to.x
void LineDrawer::DrawThinLine(const Point &from, const Point &to, Layer *layer, Color32 c)
{
    Point A = from;
    Point B = to;

    int dx = B.x - A.x;
    int inc = 1;
    int dy = B.y - A.y;
    if (dy < 0)
    {
        dy = -dy;
        inc = -1;
    }
    int eps = 0;
    int y = A.y;
    int x = A.x;
    int& X = x;
    int& Y = y;

    if (dy > dx)
    {
        int temp = dy;
        dy = dx;
        dx = temp;
        x = A.y;
        y = A.x;
        X = y;
        Y = x;
    }

    layer->reserve(dx);

    for(int i = 0; i <= dx; i++, x++)
    {
        layer->pushPixel(X,Y,c);
        eps += dy;
        if ((eps << 1) >= dx)
        {
            y += inc;
            eps -= dx;
        }
    }
}

// Assumption: from.x < to.x
void LineDrawer::DrawThickLine(const Point &from, const Point &to,
                               Layer *layer, unsigned short lw, unsigned short bw,
                               Color32 border_color, Color32 fill_color)
{
    int x1 = from.x;
    int y1 = from.y;
    int x2 = to.x;
    int y2 = to.y;
    int half_l = lw/2;
    int dx = x2 - x1;
    int dy = y2 - y1;
    int len2 = dx*dx + dy*dy;
    int len = int(sqrt(float(len2)));
    int x_width = half_l*len/dy;
    int abs_x_width = (dy > 0) ? x_width : -x_width;
    int nx = -(half_l*dy)/len;
    int ny = (half_l*dx)/len;
    int x3 = x1 + nx;
    int y3 = y1 + ny;
    int x4 = x1 - nx;
    int y4 = y1 - ny;
    int x5 = x2 + nx;
    int y5 = y2 + ny;
    int x6 = x2 - nx;
    int y6 = y2 - ny;
    int miny = std::min(y4,y6);
    int maxy = std::max(y3,y5);
    int minx = x1 - abs_x_width + ((miny - y1)*dx/dy);
    int C1 = -x3*dy + y3*dx;
    int C2 = -x4*dy + y4*dx;
    int C3 = -x3*ny + y3*nx;
    int C4 = -x5*ny + y5*nx;
    int Cy1 = minx*dy - miny*dx + C1;
    int Cy2 = minx*dy - miny*dx + C2;
    int Cy3 = minx*ny - miny*nx + C3;
    int Cy4 = minx*ny - miny*nx + C4;
    int bw2 = -1;
    if (bw > 0) bw2 = bw*bw*len2;
    for(int y = miny; y <= maxy; y++)
    {
        int Cx1 = Cy1;
        int Cx2 = Cy2;
        int Cx3 = Cy3;
        int Cx4 = Cy4;
        int x_online = x1 + ((y - y1)*dx/dy);
        for(int x = x_online - abs_x_width; x <= x_online + abs_x_width; x++ )
        {
            if ( ((Cx1 >= 0)^(Cx2 > 0)) && ((Cx3 >= 0)^(Cx4 > 0)) )
            {
                if (Cx1*Cx1 <= bw2 || Cx2*Cx2 <= bw2)
                    layer->pushPixel(x,y,border_color);
                else
                    layer->pushPixel(x,y,fill_color);
            }
            Cx1 += dy;
            Cx2 += dy;
            Cx3 += ny;
            Cx4 += ny;
        }
        Cy3 += x_width;
        Cy4 += x_width;
    }
}

void PolygonDrawer::DrawPolygon(Point *point_set, const unsigned short &nPnt, Layer *layer, ShapeStyle *style, bool isConvex)
{
    if (nPnt == 3)
    {
        DrawTriangle(point_set, layer, style);
        return;
    }
    if (isConvex)
    {
        DrawConvexPolygon(point_set,nPnt, layer, style);
    }
    else
    {
        DrawConcavePolygon(point_set,nPnt, layer, style);
    }
}

void PolygonDrawer::DrawConvexPolygon(Point *point_set, const unsigned short &nPnt, Layer *layer, ShapeStyle *style)
{

}

void PolygonDrawer::DrawConcavePolygon(Point *point_set, const unsigned short &nPnt, Layer *layer, ShapeStyle *style)
{

}

void PolygonDrawer::DrawTriangle(Point *point_set, Layer *layer, ShapeStyle *style)
{
    Point& A = point_set[0];
    Point& B = point_set[1];
    Point& C = point_set[2];
    int& x1 = A.x;
    int& x2 = B.x;
    int& x3 = C.x;
    int& y1 = A.y;
    int& y2 = B.y;
    int& y3 = C.y;
    int minx = std::min(std::min(x1,x2),x3);
    int miny = std::min(std::min(y1,y2),y3);
    int maxx = std::max(std::max(x1,x2),x3);
    int maxy = std::max(std::max(y1,y2),y3);
    int dx12 = x1 - x2;
    int dx23 = x2 - x3;
    int dx31 = x3 - x1;
    int dy12 = y1 - y2;
    int dy23 = y2 - y3;
    int dy31 = y3 - y1;
    int C1 = x1*dy12 - y1*dx12;
    int C2 = x2*dy23 - y2*dx23;
    int C3 = x3*dy31 - y3*dx31;
    int Cy1 = C1 - minx*dy12 + miny*dx12;
    int Cy2 = C2 - minx*dy23 + miny*dx23;
    int Cy3 = C3 - minx*dy31 + miny*dx31;

    int len12 = dx12*dx12 + dy12*dy12;
    int len23 = dx23*dx23 + dy23*dy23;
    int len31 = dx31*dx31 + dy31*dy31;
    int w2 = style->border_width*style->border_width;

    for(int y = miny; y <= maxy; y++)
    {
        int Cx1 = Cy1;
        int Cx2 = Cy2;
        int Cx3 = Cy3;
        for(int x = minx; x <= maxx; x++)
        {
            if (Cx1 >= 0 && Cx2 >=0 && Cx3 >=0)
            {
                if (Cx1*Cx1 <= w2*len12 || Cx2*Cx2 <= w2*len23 || Cx3*Cx3 <= w2*len31)
                    layer->pushPixel(x,y,style->pen_color);
                else
                    layer->pushPixel(x,y,style->brush_color);
            }
            Cx1 -= dy12;
            Cx2 -= dy23;
            Cx3 -= dy31;
        }
        Cy1 += dx12;
        Cy2 += dx23;
        Cy3 += dx31;
    }
}


}
