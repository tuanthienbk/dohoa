#ifndef IMAGEIO_H
#define IMAGEIO_H

#include "color.h"

// interface
class ImageIO
{
public:
    ImageIO() = default;
    virtual void read(const char* filename, Color32** row_pointer, unsigned short* width, unsigned short* height) = 0;
    virtual void write(const char* filename, Color32** row_pointer, unsigned short width, unsigned short height) = 0;
};

class ImageIO_PNG : public ImageIO
{
public:
    ImageIO_PNG() = default;
    void read(const char* filename, Color32** row_pointer, unsigned short* width, unsigned short* height);
    void write(const char* filename, Color32** row_pointer, unsigned short width, unsigned short height);
};

#endif // IMAGEIO_H
