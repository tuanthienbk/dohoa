#include <iostream>
using namespace std;

#include "drawtk.h"
using namespace dohoa2d;

int main(int argc, char **argv)
{
    Surface window(800,600);
    DrawTK* drawtk = new DrawTK;

    Layer* layer1 = new Layer;
    ShapeStyle* style1 = new ShapeStyle(0xff000000, 0xff333333, 0);
    drawtk->setLayer(layer1);
    drawtk->setStyle(style1);
    drawtk->DrawCircle(Point(200,300), 100);
    drawtk->DrawCircle(Point(600,300), 100);
    drawtk->DrawCircle(Point(400,100), 100);
    drawtk->DrawCircle(Point(400,500), 100);

    Layer* layer2 = new Layer;
    drawtk->setLayer(layer2);
    style1->pen_color = 0xffff99ff;
    style1->border_width = 10;
    drawtk->DrawRectangle(Rect(120,120,Point(140,240)));
    style1->pen_color = 0xff0000ff;
    drawtk->DrawCircle(Point(600,300), 80);
    style1->pen_color = 0xff007700;
    PolygonDrawer* ngonDrawer = new PolygonDrawer;
    drawtk->setPolygonDrawer(ngonDrawer);
    vector<Point> pnts = {Point(400,20),Point(330,140),Point(470,140)};
    drawtk->FillPolygon(pnts.data(), pnts.size());
    LineDrawer* lineDrawer = new LineDrawer;
    drawtk->setLineDrawer(lineDrawer);
    LineStyle* style2 = new LineStyle(0xff000000, 0xffff6633, 0, 20);
    drawtk->setStyle(style2);
    drawtk->DrawLine(Point(340, 440), Point(460,560));
    drawtk->DrawLine(Point(340, 560), Point(460,440));

    window.pushLayer(layer1);
    window.pushLayer(layer2);
    window.exportImage("test.png");
    return 0;
}
