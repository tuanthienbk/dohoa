#ifndef COLOR_HPP
#define COLOR_HPP

template <unsigned int Nbit, typename T>
union rgba
{
    T nbits;
    struct {
        unsigned char r: Nbit;
        unsigned char g: Nbit;
        unsigned char b: Nbit;
        unsigned char a: Nbit;
    };
    rgba() : nbits(0xff000000) {}
    rgba(T t) : nbits(t) {}
    rgba(unsigned char red, unsigned char blue, unsigned char green, unsigned char transparent)
        : r(red),
          b(blue),
          g(green),
          a(transparent)
    {}
};

typedef rgba<8, unsigned int> Color32;
typedef rgba<4, unsigned short> Color16;

template<class Color>
struct Pixel
{
    unsigned short x;
    unsigned short y;
    Color color;
    Pixel(unsigned short xc, unsigned short yc, Color c)
        : x(xc),
          y(yc),
          color(c)
    {
    }
};

typedef Pixel<Color32> Pixel32;
typedef Pixel<Color16> Pixel16;

#endif // COLOR_HPP
