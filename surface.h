#ifndef SURFACE_HPP
#define SURFACE_HPP

#include "color.h"
#include "imageio.h"

#include <vector>

namespace dohoa2d {

struct Point
{
    int x;
    int y;

    Point(int a, int b) : x(a), y(b) {}
};

struct Rect
{
    unsigned int width;
    unsigned int height;
    Point p;

    Rect(unsigned int w, unsigned int h, Point pnt)
        : width(w),
          height(h),
          p(pnt)
    {
    }
};

class Layer
{
private:
    std::vector<Pixel32> pixels;
public:
    Layer() = default;
    Layer(int numPixels);
    void reserve(const unsigned int& s);
    void pushPixel(Pixel32 pixel);
    void pushPixel(unsigned short x, unsigned short y, Color32 color);
    Pixel32& getPixelAt(unsigned int i);
    unsigned int Count();
};

class Surface
{
private:
    Rect rect;
    Color32 bg_color;
    ImageIO* imageio;
    std::vector<Layer*> layers;
public:
    Surface() = delete;
    Surface(int w, int h);
    void setRect(Rect r);
    void setBackgroundColor(Color32 c);

    void setImageIO(ImageIO* io);
    void exportImage(const char* filename);

    unsigned int getNumLayers();
    Layer* getLayerAt(unsigned int i);
    Layer* getLastLayer();
    void pushLayer(Layer* l);
    void popLayer();
};

}
#endif // SURFACE_HPP
