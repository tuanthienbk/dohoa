#ifndef DRAWSTYLE_H
#define DRAWSTYLE_H

#include "color.h"

struct Style
{
    Color32 pen_color;
    Color32 brush_color;
    Style(Color32 pen, Color32 brush)
        : pen_color(pen),
          brush_color(brush)
    {}
};

struct ShapeStyle: public Style
{
    unsigned short border_width;
    ShapeStyle(Color32 pen, Color32 brush, unsigned short bw)
               : Style(pen,brush),
               border_width(bw)
    {}
};

struct LineStyle : public ShapeStyle
{
    unsigned short line_width;
    LineStyle(Color32 pen, Color32 brush, unsigned short bw, unsigned short lw)
        : ShapeStyle(pen,brush,bw),
          line_width(lw)
    {}
};

struct ArrowStyle: public LineStyle
{
    unsigned short arrow_length;
    unsigned short arrow_width;
};

#endif // DRAWSTYLE_H
