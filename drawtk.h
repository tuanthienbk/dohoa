#ifndef DRAWTK_H
#define DRAWTK_H

#include "surface.h"
#include "drawstyle.h"

namespace dohoa2d {

enum END_CAPS_TYPE
{
    NONE = 0,
    FIRST = 1,
    SECOND = 2,
    BOTH = 4
};

class ILineDrawer
{
public:
    ILineDrawer() = default;
    virtual void DrawLine(const Point& from, const Point& to, END_CAPS_TYPE type,
                  Layer* layer, LineStyle* style)=0;
};

class LineDrawer : public ILineDrawer
{
public:
    LineDrawer() = default;
    void DrawLine(const Point& from, const Point& to, END_CAPS_TYPE type,
                  Layer* layer, LineStyle* style) override;
private:
    // Bresenham's algorithm
    void DrawThinLine(const Point& from, const Point& to, Layer* layer, Color32 c);
    // "Optimized" algorithm
    void DrawThickLine(const Point& from, const Point& to, Layer* layer,
                       unsigned short lw, unsigned short bw,
                       Color32 border_color, Color32 fill_color);
};

class IPolygonDrawer
{
public:
    virtual void DrawPolygon(Point* point_set, const unsigned short& nPnt,
                             Layer* layer, ShapeStyle* style, bool isConvex = true)=0;
};

class PolygonDrawer : public IPolygonDrawer
{
public:
    void DrawPolygon(Point* point_set, const unsigned short& nPnt,
                     Layer* layer, ShapeStyle* style, bool isConvex = true) override;
private:
    void DrawConvexPolygon(Point* point_set, const unsigned short& nPnt,
                           Layer* layer, ShapeStyle* style);
    void DrawConcavePolygon(Point* point_set, const unsigned short& nPnt,
                           Layer* layer, ShapeStyle* style);
    void DrawTriangle(Point* point_set, Layer* layer, ShapeStyle* style);
};

class DrawTK
{
private:
    Layer* layer;
    Style* style;
    ILineDrawer* line_drawer;
    IPolygonDrawer* poly_drawer;
public:
    DrawTK() = default;
    DrawTK(Layer* l, Style* s) : layer(l), style(s) {}

    void setLayer(Layer* l);
    Layer* getLayer() const;
    void setStyle(Style* s);
    Style* getStyle() const;
    void setPolygonDrawer(IPolygonDrawer* drawer);
    void setLineDrawer(ILineDrawer* drawer);

    void DrawLine(const Point& from, const Point& to, END_CAPS_TYPE type = END_CAPS_TYPE::NONE) const;
    void DrawArrow(const Point& from, const Point& to, END_CAPS_TYPE type = END_CAPS_TYPE::FIRST) const;
    void DrawRectangle(const Rect& rect) const;
    void DrawEclipse(const Point center, const unsigned short& rx, const unsigned short& ry) const;
    void DrawCircle(const Point center, const unsigned short& r) const;
    void FillPolygon(Point* point_set, const unsigned short& nPnt, bool isConvex = true) const;

};

}
#endif // DRAWTK_H
