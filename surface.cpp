#include "surface.h"
#include <iostream>

namespace dohoa2d
{

Surface::Surface(int w, int h)
    : rect(Rect(w,h,Point(0,0)))
    , bg_color(0xFFFFFFFF)
    , imageio(NULL)
{
}

void Surface::setRect(Rect r)
{
    rect = r;
}

void Surface::setBackgroundColor(Color32 c)
{
    bg_color = c;
}

void Surface::setImageIO(ImageIO *io)
{
    imageio = io;
}

void Surface::exportImage(const char *filename)
{
    Color32** pixel_p = new Color32*[rect.height];
    for(int i = 0; i <  rect.height; i++)
    {
        pixel_p[i] = new Color32[rect.width];
        for(int j = 0; j <  rect.width; j++)
        {
            pixel_p[i][j] = bg_color;
        }
    }

    for(int i = 0; i < layers.size(); i++)
    {
        for(int j = 0; j < layers[i]->Count(); j++)
        {
            Pixel32& pixel = layers[i]->getPixelAt(j);
            if (pixel.x >= 0 && pixel.x < rect.width &&
                    pixel.y >= 0 && pixel.y < rect.height)
                pixel_p[pixel.y][pixel.x] = pixel.color;
        }
    }
    if (!imageio)
        imageio = new ImageIO_PNG();

    imageio->write(filename, pixel_p,rect.width, rect.height);

    for(int i = 0; i <  rect.height; i++)
        delete[] pixel_p[i];
}

unsigned int Surface::getNumLayers()
{
    return layers.size();
}

Layer *Surface::getLayerAt(unsigned int i)
{
    return layers.at(i);
}

Layer *Surface::getLastLayer()
{
    return layers.back();
}

void Surface::pushLayer(Layer *l)
{
    layers.push_back(l);
}

void Surface::popLayer()
{
    if (!layers.empty())
        layers.pop_back();
}

Layer::Layer(int numPixels)
{
    pixels.reserve(numPixels);
}

void Layer::reserve(const unsigned int &s)
{
    pixels.reserve(s);
}

void Layer::pushPixel(Pixel32 pixel)
{
    pixels.push_back(pixel);
}

void Layer::pushPixel(unsigned short x, unsigned short y, Color32 color)
{
    pixels.push_back(Pixel32(x,y,color));
}

Pixel32 &Layer::getPixelAt(unsigned int i)
{
    return pixels.at(i);
}

unsigned int Layer::Count()
{
    return pixels.size();
}


}
